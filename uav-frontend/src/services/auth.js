import axios from 'axios';

export default {
  async login(credentials) {
    const response = await axios.post('/login', credentials, { withCredentials: true });
    return response.data;
  },
  async logout() {
    await axios.post('/logout', {}, { withCredentials: true });
  }
};
