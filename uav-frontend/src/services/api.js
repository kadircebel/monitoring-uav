import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:5000/api', // Flask API'nizin base URL'si
});

export default api;
