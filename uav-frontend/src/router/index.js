import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import CreateView from '@/views/Drones/CreateView.vue';
import DroneIndex from '@/views/Drones/DroneIndex.vue';
import EditView from '@/views/Drones/EditView.vue';
import TaskIndex from '@/views/Tasks/TaskIndex.vue';
import TaskCreate from '@/views/Tasks/CreateView.vue';
import TaskEdit from '@/views/Tasks/TaskEditView.vue';
import TaskDetail from '@/views/Tasks/TaskDetailView.vue';
import TaskExecute from '@/views/Tasks/TasksExecuteView.vue';
import LoginView from '@/views/LoginView.vue';
import RegisterView from '@/views/RegisterView.vue';
// import axios from 'axios';


const routes = [
    {
        path: '/',
        name: 'HomeView',
        component: HomeView,
        meta: { requiresAuth: true },
    },
    {
        path: '/drones/',
        name: 'DroneIndex',
        component: DroneIndex,
        meta: { requiresAuth: true },

    },
    {
        path: '/drones/create',
        name: 'CreateView',
        component: CreateView,
        meta: { requiresAuth: true },
    },
    {
        path: '/drones/:id/edit',
        name: 'EditView',
        component: EditView,
        props: true,
        meta: { requiresAuth: true },
    },
    {
        path: '/tasks/',
        name: 'TaskIndex',
        component: TaskIndex,
        meta: { requiresAuth: true },
    },
    {
        path: '/tasks/create',
        name: 'TaskCreate',
        component: TaskCreate,
        meta: { requiresAuth: true },
    },
    {
        path: '/tasks/:id/edit',
        name: 'TaskEdit',
        component: TaskEdit,
        props: true,
        meta: { requiresAuth: true },
    },
    {
        path: '/tasks/:id/detail',
        name: 'TaskDetail',
        component: TaskDetail,
        props: true,
        meta: { requiresAuth: true },
    },
    {
        path: '/tasks/:id/execute',
        name: 'TaskExecute',
        component: TaskExecute,
        props: true,
        meta: { requiresAuth: true },
    },
    {
        path: '/login',
        name: 'LoginView',
        component: LoginView,
    },
    {
        path: '/register',
        name: 'RegisterView',
        component: RegisterView,
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

router.beforeEach((to, from, next) => {
    // Check if the route requires authentication
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // Check if user is authenticated
      const token = localStorage.getItem('token');
      if (!token) {
        // Redirect to login if not authenticated
        next({ name: 'LoginView' });
      } else {
        // Continue to the route
        next();
      }
    } else {
      // Continue to the route
      next();
    }
  });

export default router;