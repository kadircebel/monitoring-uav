import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';
// import api from './services/api';

const app = createApp(App);

app.config.globalProperties.$http = axios;
axios.defaults.baseURL = 'http://localhost:5000/api';


app.use(router);
app.mount('#app');
