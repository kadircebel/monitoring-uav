# from flask_sqlalchemy import SQLAlchemy
from minio import Minio
from configs import Config
import random, os
from PIL import Image


minio_client = Minio(
    endpoint=Config.MINIO_ENDPOINT,
    access_key=Config.MINIO_ACCESS_KEY,
    secret_key=Config.MINIO_SECRET_KEY,
    secure=Config.MINIO_SECURE
    )



# Rastgele noisy image oluşturma
def generate_noisy_image(width, height):
    image = Image.new('RGB', (width, height))
    pixels = image.load()
    for i in range(width):
        for j in range(height):
            r = random.randint(0, 255)
            g = random.randint(0, 255)
            b = random.randint(0, 255)
            pixels[i, j] = (r, g, b)
    return image


# Save image to static folder
def save_image_to_static(image, image_name):
    from app import app

    static_folder = app.static_folder
    if not os.path.exists(static_folder):
        os.makedirs(static_folder)
    
    image_path = os.path.join(static_folder, image_name)
    image.save(image_path)