from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate
from configs import Config
from extensions import *
from flask_jwt_extended import JWTManager


app = Flask(__name__)

CORS(app, supports_credentials=True)

app.config['SQLALCHEMY_DATABASE_URI'] = Config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = Config.SQLALCHEMY_TRACK_MODIFICATIONS
app.config["JWT_TOKEN_LOCATION"] = ["headers", "cookies", "json", "query_string"]
app.config["JWT_COOKIE_SECURE"] = False
app.config['JWT_SECRET_KEY'] = Config.JWT_SECRET_KEY

db = SQLAlchemy(app)
jwt = JWTManager(app)

migrate = Migrate(app, db)
from routes import *

bucket_name = Config.MINIO_BUCKET_NAME

if not minio_client.bucket_exists(bucket_name):
    minio_client.make_bucket(bucket_name)



if __name__ == '__main__':
    app.run(debug=True)
