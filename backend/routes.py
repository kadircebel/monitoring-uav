from app import db,app
from flask import request, jsonify, url_for, make_response
from models import Drone, Task, Image, User
import random, string, io, os
from PIL import Image
from minio.error import S3Error
from configs import Config
from extensions import minio_client, generate_noisy_image, save_image_to_static
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token, jwt_required,get_jwt_identity




bucket_name = Config.MINIO_BUCKET_NAME

if not minio_client.bucket_exists(bucket_name):
    minio_client.make_bucket(bucket_name)


# DRONE
@app.route('/api/drones', methods=['GET'])
def get_drones():
    """
    drone list
    """
    drones = Drone.query.all()
    drone_list = []
    for item in drones:
        drone_list.append({
            'id':item.id,
            'name':item.name
        })
    return jsonify(drone_list)

@app.route('/api/drones', methods=['POST'])
def create_drone():
    """
    create a new drone
    """
    data = request.get_json()
    new_drone = Drone(name=data['name'])
    db.session.add(new_drone)
    db.session.commit()
    return jsonify({'id':new_drone.id,'name':new_drone.name})

@app.route('/api/drones/<int:id>', methods=['GET'])
def get_drone(id):
    """
    select drone
    """
    drone = Drone.query.get(id)
    if not drone:
        return jsonify({'message':'Drone not found!'}), 404
    
    return jsonify(drone.to_dict()), 200

@app.route('/api/drones/<int:id>', methods=['PUT'])
def update_drone(id):
    """
    update drone
    """
    drone = Drone.query.get(id)
    if not drone:
        return jsonify({'message': 'Drone not found'}), 404

    data = request.json
    drone.name = data['name']

    try:
        db.session.commit()
        return jsonify(drone.to_dict()), 200
    except Exception as e:
        db.session.rollback()
        return jsonify({'message': 'Failed to update drone', 'error': str(e)}), 500
    finally:
        db.session.close()

@app.route('/api/drones/<int:id>', methods=['DELETE'])
def delete_drone(id):
    """
    delete drone
    """
    drone = Drone.query.get(id)
    if not drone:
        return jsonify({'message': 'Drone not found'}), 404

    try:
        db.session.delete(drone)
        db.session.commit()
        return jsonify({'message': 'Drone deleted successfully'}), 200
    except Exception as e:
        db.session.rollback()
        return jsonify({'message': 'Failed to delete drone', 'error': str(e)}), 500
    finally:
        db.session.close()


# TASK
@app.route('/api/tasks', methods=['POST'])
def create_task():
    data = request.json
    task_name = data.get('name')
    task_description = data.get('description')
    drone_ids = data.get('drones', [])

    try:
        new_task = Task(name=task_name, description=task_description)
        # db.session.commit()

        print("drones : {}".format(drone_ids))
        
        # Add drones to the task
        for drone_id in drone_ids:
            drone = Drone.query.get(drone_id)
            print("selected : {}".format(drone))
            if drone:
                new_task.drones.append(drone)
                print(f"Drone with ID {drone_id} added to task {new_task.id}")

        db.session.add(new_task)
        db.session.commit()

        return jsonify({'message': 'Task created successfully'}), 201
    except Exception as e:
        db.session.rollback()
        return jsonify({'message': 'Failed to create task.', 'error': str(e)}), 500
    finally:
        db.session.close()


@app.route('/api/tasks', methods=['GET'])
def get_tasks():
    tasks = Task.query.all()
    return jsonify([task.to_dict() for task in tasks])


@app.route('/api/tasks/<int:task_id>', methods=['PUT'])
def update_task(task_id):
    data = request.json
    task_name = data.get('name')
    task_description = data.get('description')
    drone_ids = data.get('drones', [])

    try:
        
        task = Task.query.get_or_404(task_id)

        if task_name:
            task.name = task_name
        if task_description:
            task.description = task_description

        # Clear existing drones
        task.drones.clear()

        # Add updated drones to the task
        for drone_id in drone_ids:
            drone = Drone.query.get(drone_id)
            if drone:
                task.drones.append(drone)

        db.session.commit()

        return jsonify({'message': 'Task updated successfully'}), 200
    except Exception as e:
        db.session.rollback()
        return jsonify({'message': 'Failed to update task.', 'error': str(e)}), 500
    finally:
        db.session.close()


@app.route('/api/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = Task.query.get_or_404(task_id)
    return jsonify(task.to_dict())

@app.route('/api/tasks/<int:id>/execute', methods=['POST'])
def execute_task(id):
    task = Task.query.get_or_404(id)
    images = []

    for i in range(5):
        noisy_image_data = generate_noisy_image(640, 480)
        image_name = f'task_{id}_image_{i + 1}.jpg'        
        try:
            save_image_to_static(noisy_image_data, image_name)

            with open(os.path.join(app.static_folder, image_name), 'rb') as image_file:
                minio_client.put_object(
                    bucket_name=Config.MINIO_BUCKET_NAME,
                    object_name=image_name,
                    data=image_file,
                    length=os.stat(image_file.name).st_size,
                    content_type='image/jpeg'
                )

            image_url = url_for('static', filename=image_name, _external=True)
            images.append({'name': image_name, 'url': image_url})
        except S3Error as err:
            print(f"Error uploading image to MinIO: {err}")
            return jsonify({'message': f'Failed to upload image: {err}'}), 500

    return jsonify({'message': 'Task executed successfully', 'images': images }), 200

@app.route('/api/tasks/<int:id>/images', methods=['GET'])
def get_task_images(id):
    task = Task.query.get_or_404(id)
    image_names = []
    image_urls = []

    for i in range(5):
        image_name = f'task_{id}_image_{i + 1}.jpg'
        image_names.append(image_name)
        try:
            # Presigned URL oluştur
            url = minio_client.presigned_get_object(Config.MINIO_BUCKET_NAME, image_name)
            image_urls.append(url)
        except S3Error as err:
            print(f"Error generating presigned URL for {image_name}: {err}")

    return jsonify({'task_id': id, 'images': image_urls}), 200


@app.route('/api/register', methods=['POST'])
def register():
    data = request.get_json()
    hashed_password = generate_password_hash(data['password'], method='pbkdf2:sha256')
    new_user = User(username=data['username'], password=hashed_password)
    db.session.add(new_user)
    db.session.commit()
    return jsonify({'message': 'User registered successfully'}), 201

@app.route('/api/login', methods=['POST'])
def login():
    data = request.get_json()
    user = User.query.filter_by(username=data['username']).first()
    if user and check_password_hash(user.password, data['password']):
        access_token = create_access_token(identity=user.username)
        return jsonify({'token': access_token}), 200
    return jsonify({'message': 'Invalid credentials'}), 401

@app.route('/api/protected', methods=['GET'])
@jwt_required()
def protected():
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200

@app.route('/api/logout', methods=['POST'])
def logout():
    response = jsonify({"message": "Successfully logged out"})
    response.delete_cookie('access_token')
    return response, 200

# # Login endpoint
# @app.route('/api/login', methods=['POST'])
# def login():
#     data = request.get_json()
#     username = data.get('username')
#     password = data.get('password')

#     user = User.query.filter_by(username=username).first()
#     if user and check_password_hash(user.password, password):
#         access_token = create_access_token(identity=user.username)
#         # Set token as a JSON response
#         return jsonify({'token': access_token}), 200
#     return jsonify({'message': 'Invalid credentials'}), 401

# # Auth check endpoint
# @app.route('/api/auth-check', methods=['GET'])
# @jwt_required()
# def auth_check():
#     current_user = get_jwt_identity()
#     return jsonify(logged_in_as=current_user), 200

# # Logout endpoint
# @app.route('/api/logout', methods=['POST'])
# @jwt_required()
# def logout():
#     response = jsonify({"message": "Successfully logged out"})
#     response.delete_cookie('access_token')
#     return response, 200