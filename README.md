# UAV Monitoring and Task Execution Platform

## Overview

This project is a web-based platform designed for UAV (Unmanned Aerial Vehicle) monitoring and task execution. It allows users to manage drone tasks, execute tasks, and retrieve images captured during task execution.

## Features

### Backend (Python Flask)

* Flask application serving as the backend.
* RESTful API endpoints for managing drones, tasks, and images.
* SQLAlchemy ORM used for database interaction.

* API Endpoints:
  * "**GET /api/drones** " : Retrieve a list of drones with basic information.
  * "**POST /api/tasks** " : Create a new task with task details and assigned drone(s).
  * "**GET /api/tasks/:id** " : Retrieve details of a specific task, including associated drone(s).
  * "**POST /api/tasks/:id/execute** " : Execute a task, triggering image capture by assigned drones (5 random noisy images generated).
  * "**GET /api/tasks/:id/images** " : Retrieve images captured during task execution.

* Database Schema:
  * Four tables: Drone, Task, Image, User
  * One-to-many relationship between Drone and Task tables.
  * Foreign key constraints and data integrity ensured.

* Database Type:
  * SqlLite

### Frontend (Vue.js)
* Vue.js application for the frontend.
* Interfaces for creating tasks, assigning drones, and executing tasks.
* Page for displaying task details, including associated drones and captured images.
* Utilizes fetch API for dynamic updates from the backend.

### Task Execution and Image Capture
* Functionality to execute tasks assigned to drones.
* Image capture by drones (dummy noisy images generated) during task execution.
* Captured images associated with corresponding tasks for easy retrieval.

## Setup Instructions

### Clone Repository

```bash
mkdir <parent_folder>
python3 -m venv venv
source venv/bin/activate
git clone git@gitlab.com:kadircebel/monitoring-uav.git
```
### Setup
```bash
npm install
pip install -r requirements.txt
npm install @vue/cli --save-dev 

wget https://dl.min.io/server/minio/release/linux-amd64/archive/minio_20240613225353.0.0_amd64.deb -O minio.deb
sudo dpkg -i minio.deb

mkdir ~/minio
minio server ~/minio --console-address :9001

# Backend will run on http://localhost:5000.
flask run
# Frontend will run on http://localhost:8080.
npm run serve

```
> You have to install "minIO" on your computer and activate it to capture an image when executing the task.

### Accessing the Application

* Open your web browser and go to http://localhost:8080 to access the UAV monitoring platform.
* you can register (http://localhost:8080/register) a new user for access to cms panel or you can login with admin-1234567
* Use the provided interfaces to create tasks, assign drones, execute tasks, and retrieve captured images.

## License

[MIT](https://choosealicense.com/licenses/mit/)